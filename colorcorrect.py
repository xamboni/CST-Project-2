'''
Title: Color-Correct, it is a work in progress. 
Copy image into main4.py file location, convert to a jpg, and rename it '1a.jpg'

Abstract: Image is altered to and from a specific color blindness. 
The algorithm used to simulate color blindness is based upon a color matrix found here:
http://web.archive.org/web/20081014161121/http://www.colorjack.com/labs/colormatrix/
This author color matrix is based on Daltonize.org. 

The general idea is re-create a RGB color channel mixer at the pixel level. Convert the image to 
a color blind image and reverse the process.An image is split into its RGB band. From there, 
convert the single band to a RGB, three bands, and split them again. 
Adjustments are made to each image band as per the color matrix. The single band image are layered,
with transparency as a mask, thanks to the female TA for suggesting this. Next, the image's brightness 
levels are raised/lowered as to account for darken results from the color matrix. 

Testing the images for correctness was performed by using a color blind simulator.
http://www.color-blindness.com/coblis-color-blindness-simulator/

The color blind converter works on blue-blind and red-blind. As for the other two, green is to dominate.
Monochromacy is replaced with an opencv gray scale built in feature.

All of the adjustments DO NOT work, yet. The reciprocal of the color matrix is applied to the color blind images, 
but the results are out of range of 255. First approach would be to normalize the images by each bands. 
Second, change the sequence of layering based on the color blindness instead of universal layering.
Additional work would include a dampening for the green color in the green-blind and monochromacy. Perhaps
a various color matrix from daltonize.org would do. Although there are Daltonize tranformation matrices.

As suggested by vischeck.com, the quick fix is to increase the red and green contrast as to make them more distinct. 
Also, they offer another approach that is to, "convert these (color blind type) into changes in brightness, 
and blue/yellow coloration." This would give a new color depth which would help identify the color.
 

Author: Phillip T. Emmons
Alas, outside of a few explanations from Dylan and Chris, this was written by me.

Date: 10.14.2016

Protanopia:{ R:[56.667, 43.333, 0], G:[55.833, 44.167, 0], B:[0, 24.167, 75.833]}
Deuteranopia:{ R:[62.5, 37.5, 0], G:[70, 30, 0], B:[0, 30, 70]}
Tritanopia:{ R:[95, 5, 0], G:[0, 43.333, 56.667], B:[0, 47.5, 52.5]}
Achromatopsia:{ R:[29.9, 58.7, 11.4], G:[29.9, 58.7, 11.4], B:[29.9, 58.7, 11.4]}

'''
from PIL import Image
import numpy as np
import cv2

def minMax( value ):#The adjusted image's color range were out of bounds.
    if value> 255:
        return 255
    if value< 0:
        return 0
    else: return int(value)
''' 
**future work**   
#def normalizeMinMax( value ):
http://stackoverflow.com/questions/29661574/normalize-numpy-array-columns-in-python
  import numpy as np

x = np.array([[1000,  10,   0.5],
              [ 765,   5,  0.35],
              [ 800,   7,  0.09]])

x_normed = x / x.max(axis=0)

print(x_normed)
# [[ 1.     1.     1.   ]
#  [ 0.765  0.5    0.7  ]
#  [ 0.8    0.7    0.18 ]]  
'''
#Each RGB band image is multiplied by it corresponding color blind factor.
#The saved files are used by opencv.
imRGB = Image.open("1a.jpg")
width, height = imRGB.size
def pixelLand(wide, tall, colorImRGB, filename, colorMult, row):
    
    for x in range( 0, wide ):
        for y in range( 0, tall):
            r_rpix, g_rpix, b_rpix = colorImRGB.getpixel((x,y))
            rr = minMax(r_rpix * colorMult[row][0])
            gr = minMax(g_rpix * colorMult[row][1])
            br = minMax(b_rpix * colorMult[row][2])
            colorImRGB.putpixel((x,y), (rr,gr,br))
        
    colorImRGB.save(filename) 



#This is a check to see if the correct image is being processed.
def Normal():
    imCV = cv2.imread('1a.jpg')
    cv2.imshow('Normal',imCV)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
''' The four converters use the same format, which could be written into single method in the future.'''    
def Prota():
#color matrix
    Protanopia= [ [.56667, .43333, 0], 
                  [.55833, .44167, 0], 
                  [0, .24167, .75833] ]
#Converting the single band into three bands to be altered.
    rimRGB = Image.open("rim.jpg").convert('RGB')
    gimRGB = Image.open("gim.jpg").convert('RGB')
    bimRGB = Image.open("bim.jpg").convert('RGB')
    
#Apply the matrix to the image.
    pixelLand(width, height, rimRGB, 'rimP.jpg', Protanopia, 0)
    pixelLand(width, height, gimRGB, 'gimP.jpg', Protanopia, 1)
    pixelLand(width, height, bimRGB, 'bimP.jpg', Protanopia, 2)
    
#openCV has features for transparency and merging two images at a time.
    rimCV = cv2.imread('rimP.jpg')
    gimCV = cv2.imread('gimP.jpg')
    bimCV = cv2.imread('bimP.jpg')
    
    dst2 = cv2.addWeighted(rimCV,.6,gimCV,.6,0)
    dst1 = cv2.addWeighted(dst2,.6,bimCV,.6,0)
#Alpha is the brightness - range: -3 to -3
    alpha = float(2.0)
    new_img = cv2.multiply(dst1,np.array([alpha]))
#Result to console and save file to be uploaded.
    cv2.imshow('Red-Blind/Protanopia',new_img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()  
    cv2.imwrite('Protanopia.jpg', new_img)
        
def Deute():#To much green...
    Deuteranopia= [ [.625, .375, 0], 
                  [.70, .30, 0], 
                  [0, .30, .70] ]
    
    rimRGB = Image.open("rim.jpg").convert('RGB')
    gimRGB = Image.open("gim.jpg").convert('RGB')
    bimRGB = Image.open("bim.jpg").convert('RGB')

    pixelLand(width, height, rimRGB, 'rimP.jpg', Deuteranopia, 0)
    pixelLand(width, height, gimRGB, 'gimP.jpg', Deuteranopia, 1)
    pixelLand(width, height, bimRGB, 'bimP.jpg', Deuteranopia, 2)
    
    rimCV = cv2.imread('rimP.jpg')
    gimCV = cv2.imread('gimP.jpg')
    bimCV = cv2.imread('bimP.jpg')
    
    dst2 = cv2.addWeighted(rimCV,.6,gimCV,.6,0)
    dst1 = cv2.addWeighted(dst2,.6,bimCV,.6,0)

    alpha = float(2.0)
    new_img = cv2.multiply(dst1,np.array([alpha]))

    cv2.imshow('Green-Blind/Deuteranopia',new_img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()  
    cv2.imwrite('Deuteranopia.jpg', new_img)
   
def Trita():
    Tritanopia= [ [.95, .05, 0], 
                  [0, .43333, .56667], 
                  [0, .475, .525] ]
    
    rimRGB = Image.open("rim.jpg").convert('RGB')
    gimRGB = Image.open("gim.jpg").convert('RGB')
    bimRGB = Image.open("bim.jpg").convert('RGB')

    pixelLand(width, height, rimRGB, 'rimP.jpg', Tritanopia, 0)
    pixelLand(width, height, gimRGB, 'gimP.jpg', Tritanopia, 1)
    pixelLand(width, height, bimRGB, 'bimP.jpg', Tritanopia, 2)
    
    rimCV = cv2.imread('rimP.jpg')
    gimCV = cv2.imread('gimP.jpg')
    bimCV = cv2.imread('bimP.jpg')
    
    dst2 = cv2.addWeighted(rimCV,.6,gimCV,.6,0)
    dst1 = cv2.addWeighted(dst2,.6,bimCV,.6,0)

    alpha = float(2.0)
    new_img = cv2.multiply(dst1,np.array([alpha]))

    cv2.imshow('Blue-Blind/Tritanopia',new_img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()  
    cv2.imwrite('Tritanopia.jpg', new_img)
 
def Achro():#To much green....
    #This image should be slightly brighter than a stock greyscale.
    imCV = cv2.imread('1a.jpg')
    imACH = cv2.cvtColor(imCV, cv2.COLOR_BGR2GRAY)
    '''
    Achromatopsia= [ [.299, .587, .114], 
                     [.299, .587, .114], 
                     [.299, .587, .114] ]
    
    rimRGB = Image.open("rim.jpg").convert('RGB')
    gimRGB = Image.open("gim.jpg").convert('RGB')
    bimRGB = Image.open("bim.jpg").convert('RGB')

    pixelLand(width, height, rimRGB, 'rimP.jpg', Achromatopsia, 0)
    pixelLand(width, height, gimRGB, 'gimP.jpg', Achromatopsia, 1)
    pixelLand(width, height, bimRGB, 'bimP.jpg', Achromatopsia, 2)
    
#pil to cv2
    rimCV = cv2.imread('rimP.jpg')
    gimCV = cv2.imread('gimP.jpg')
    bimCV = cv2.imread('bimP.jpg')
#the green values are to high    
    cv2.imshow('Monochromacy',rimCV)
    cv2.waitKey(0)
    cv2.destroyAllWindows() 
    
    dst2 = cv2.addWeighted(rimCV,.6,gimCV,.6,0)
    dst1 = cv2.addWeighted(dst2,.6,bimCV,.6,0)

    alpha = float(2.0)
    new_img = cv2.multiply(dst1,np.array([alpha]))
    '''
    cv2.imshow('Monochromacy',imACH)
    cv2.waitKey(0)
    cv2.destroyAllWindows()  
    cv2.imwrite('Achromatopsia.jpg', imACH)

'''Attempt to reverse the process, step by step.
   The following three functions do the same thing.'''   
def AdjProta():
#This value will be used to darken the image.
    alpha = float(0.5)
    AdjPro= [ [1.78, 2.72, 0], 
              [1.79, 2.72, 0], 
              [0, 4.12, 1.32] ]
#RGB band images are 
    bwList = []
#This file is the color blind version and darken it.    
    proCV = cv2.imread("Protanopia.jpg")
    imCV = cv2.multiply(proCV, np.array([alpha]))
    cv2.imwrite('improCV.jpg', imCV)
#Pil split to the resuce.
    imPRO = Image.open('improCV.jpg')
    width, height = imPRO.size

#Split the image for the second time into RGB channels to be processed.
    rimPRO, gimPRO, bimPRO = imPRO.split()
    bwList.extend( (rimPRO, gimPRO, bimPRO) )
    
    for i in range( 0,3 ):
        bwList[i].convert('RGB')
        bwList[i].save( str(i)+"PRO.jpg")
#Convert from band L to RGB    
    rimRGB = Image.open("0PRO.jpg").convert('RGB')
    gimRGB = Image.open("1PRO.jpg").convert('RGB')
    bimRGB = Image.open("2PRO.jpg").convert('RGB')
    
#The range of color adjust is way off.
    pixelLand(width, height, rimRGB, 'rimP.jpg', AdjPro, 0)
    pixelLand(width, height, gimRGB, 'gimP.jpg', AdjPro, 1)
    pixelLand(width, height, bimRGB, 'bimP.jpg', AdjPro, 2)
#Images are altered and saved.   
    rimCV = cv2.imread('rimP.jpg')
    gimCV = cv2.imread('gimP.jpg')
    bimCV = cv2.imread('bimP.jpg')
#'Merged into a single image.    
    dst2 = cv2.addWeighted(rimCV,.6,gimCV,.6,0)
    dst1 = cv2.addWeighted(dst2,.6,bimCV,.6,0)
#Brightened and this is were the image contrast would occur.
    alpha = float(1.0)
    new_img = cv2.multiply(dst1,np.array([alpha]))
#Result displayed and saved.
    cv2.imshow('Adjusted Protanopia',new_img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()  
    cv2.imwrite('uploadMe.jpg', new_img)
   
def AdjDeute():
    alpha = float(0.5)
    AdjDeu= [ [1.6, 2.67, 0 ], 
              [1.43, 3.33, 0], 
              [0, 3.33, 1.43] ]
    bwList = []
    
    deuCV = cv2.imread("Deuteranopia.jpg")
    imCV = cv2.multiply(deuCV, np.array([alpha]))
    cv2.imwrite('impdeuCV.jpg', imCV)
    
    imDEU = Image.open('impdeuCV.jpg')
    width, height = imDEU.size

    #split the image into RGB channels
    rimDEU, gimDEU, bimDEU = imDEU.split()
    bwList.extend( (rimDEU, gimDEU, bimDEU) )
    
    for i in range( 0,3 ):
        bwList[i].convert('RGB')
        bwList[i].save( str(i)+"DEU.jpg")
    
    rimRGB = Image.open("0DEU.jpg").convert('RGB')
    gimRGB = Image.open("1DEU.jpg").convert('RGB')
    bimRGB = Image.open("2DEU.jpg").convert('RGB')
    
    #****** the range of color adjust is way off  ***
    
    pixelLand(width, height, rimRGB, 'rimP.jpg', AdjDeu, 0)
    pixelLand(width, height, gimRGB, 'gimP.jpg', AdjDeu, 1)
    pixelLand(width, height, bimRGB, 'bimP.jpg', AdjDeu, 2)
    
#pil to cv2
    rimCV = cv2.imread('rimP.jpg')
    gimCV = cv2.imread('gimP.jpg')
    bimCV = cv2.imread('bimP.jpg')
    
    dst2 = cv2.addWeighted(rimCV,.6,gimCV,.6,0)
    dst1 = cv2.addWeighted(dst2,.6,bimCV,.6,0)

    alpha = float(1.0)
    new_img = cv2.multiply(dst1,np.array([alpha]))

    cv2.imshow('Adjusted Deuteranopia',new_img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()  
    cv2.imwrite('uploadMe.jpg', new_img)

def AdjTrita():
    alpha = float(0.5)
    AdjTri= [ [1.78, 2.72, 0], 
              [1.79, 2.72, 0], 
              [0, 4.12, 1.32] ]
    bwList = []
    
    triCV = cv2.imread("Tritanopia.jpg")
    imCV = cv2.multiply(triCV, np.array([alpha]))
    cv2.imwrite('imtriCV.jpg', imCV)
    
    imTRI = Image.open('imtriCV.jpg')
    width, height = imTRI.size

    #split the image into RGB channels
    rimTRI, gimTRI, bimTRI = imTRI.split()
    bwList.extend( (rimTRI, gimTRI, bimTRI) )
    
    for i in range( 0,3 ):
        bwList[i].convert('RGB')
        bwList[i].save( str(i)+"TRI.jpg")
    
    rimRGB = Image.open("0TRI.jpg").convert('RGB')
    gimRGB = Image.open("1TRI.jpg").convert('RGB')
    bimRGB = Image.open("2TRI.jpg").convert('RGB')
    
    #****** the range of color adjust is way off  ***
    
    pixelLand(width, height, rimRGB, 'rimP.jpg', AdjTri, 0)
    pixelLand(width, height, gimRGB, 'gimP.jpg', AdjTri, 1)
    pixelLand(width, height, bimRGB, 'bimP.jpg', AdjTri, 2)
    
#pil to cv2
    rimCV = cv2.imread('rimP.jpg')
    gimCV = cv2.imread('gimP.jpg')
    bimCV = cv2.imread('bimP.jpg')
    
    dst2 = cv2.addWeighted(rimCV,.6,gimCV,.6,0)
    dst1 = cv2.addWeighted(dst2,.6,bimCV,.6,0)

    alpha = float(1.0)
    new_img = cv2.multiply(dst1,np.array([alpha]))

    cv2.imshow('Adjusted Tritanopia',new_img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()  
    cv2.imwrite('uploadMe.jpg', new_img)
    
def AdjAchro():
    #greyscale - [.3,.59,.11]
    Normal()

'''****MAIN****'''
# Python's version of a switch-case to be used in the menu selection.
UserSelect = {
    1: Normal,
    2: Prota,
    3: Deute,
    4: Trita,
    5: Achro,
    6: AdjProta,
    7: AdjDeute,
    8: AdjTrita,
    9: AdjAchro
}

def colorCorrect():
	userInput =0
	while(userInput != 10):
		print("Please choose one of the following: ")
		print("1 - Normal Color Vision")
		print("2 - Red-Blind/Protanopia image")
		print("3 - Green-Blind/Deuteranopia image")
		print("4 - Blue-Blind/Tritanopia image")
		print("5 - Monochromacy/Achromatopsia image")
		print("6 - Adjusted Protanopia image")
		print("7 - Adjusted Deuteranopia")
		print("8 - Adjusted Tritanopia image")
		print("9 - Adjusted Achromatopsia image")
		print("10 - Exit image")

		userInput = int(input("Enter your choice: "))
		if (0< userInput) and (userInput <10):

		    #imRGB = Image.open("1a.jpg")
		    #width, height = imRGB.size

		    rgbLIST =[]

		    rim, gim, bim = imRGB.split()
		    rgbLIST.extend( (rim, gim, bim) )

		    for i in range( 0,3 ):
		        rgbLIST[i].convert('RGB')

		    rgbLIST[0].save("rim.jpg")
		    rgbLIST[1].save("gim.jpg")
		    rgbLIST[2].save("bim.jpg")

		    UserSelect[userInput]()


'''Moved these two lines from the BLAH function.
   This will make the program callable from CLI.'''

#BLAH()
#EOF